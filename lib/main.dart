import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}


class _MyHomePageState extends State<MyHomePage> {
  double scrolledPixels = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Widget of the week 11'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text('Scrolled pixels: ${scrolledPixels.toStringAsFixed(0)}', style: TextStyle(
            fontSize: 30
          ),),
          Container(
            height:  350,
            child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification notification) {
                setState(() {
                  scrolledPixels = notification.metrics.pixels;
                });
              },
              child: ListView(
                itemExtent: MediaQuery.of(context).size.height * 0.1,
                children: [
                  ...List.generate(
                    10,
                    (index) => Container(
                      margin: EdgeInsets.symmetric(vertical: 10.0),
                      color: Colors.green,
                      child: Center(
                        child: Text(
                          index.toString(),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          FutureBuilder(
              future: Future.delayed(Duration(milliseconds: 3000)),
              builder: (context, snapshot){
                if(snapshot.connectionState == ConnectionState.done) {
                  return Container(
                    height: 250,
                    width: 250,
                    child: Image.network('https://www.unqpost.com/wp-content/uploads/2020/11/af-300x300.jpg', fit: BoxFit.contain,),
                  );
                } else {
                  return Text('Wait for 3 seconds');
                }
          })
        ],
      ),
    );
  }
}
